'use strict';

var app = angular.module('cubbyApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'config',
  'xeditable',
  'angularFileUpload'
]);

app.config(function ($routeProvider, $httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  $routeProvider
    .when('/', {
      templateUrl: 'views/vitrine.html',
      controller: 'VitrineCtrl',
      css: 'styles/vitrine.css'
    })
    .when('/profile', {
      templateUrl: 'views/profile.html',
      controller: 'ProfileCtrl',
      css: 'styles/app.css'
    })
    .when('/plans', {
      templateUrl: 'views/plans.html',
      controller: 'PlansCtrl',
      css: 'styles/app.css'
    })
    .when('/space', {
      templateUrl: 'views/space.html',
      controller: 'SpaceCtrl',
      css: 'styles/app.css'
    })
    .when('/partage', {
      templateUrl: 'views/partage.html',
      controller: 'PartageCtrl',
      css: 'styles/app.css'
    })
    .when('/admin', {
      templateUrl: 'views/admin.html',
      controller: 'AdminCtrl',
      css: 'styles/app.css'
    })
    .otherwise({
      redirectTo: '/'
    });
})
  .run(function(editableOptions) {
    editableOptions.theme = 'bs3';

  });
