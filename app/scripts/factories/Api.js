'use strict';

app.factory('User', function($resource, ENV){
  return $resource(
    ENV.apiEndpoint + '/api/users/:id',
    {id: '@id'},
    {
      'get':    {method:'GET'},
      'save':   {method:'POST'},
      'query':  {method:'GET', isArray:true},
      'remove': {method:'DELETE'},
      'custom': {method:'GET'}
    });
});

app.factory('Api', function($resource, ENV) {
  return $resource(
    ENV.apiEndpoint + '/api/:f',
    {f: '@f'},
    {
      'update':   {method: 'PUT'}
    }
  );
});

app.factory('Link', function($resource, ENV) {
  return $resource(
    ENV.apiEndpoint + '/api/links/:f',
    {f: '@f'},
    {
      'get':   {method: 'GET'},
      'save':  {method: 'POST'},
      'query': {method: 'GET', isArray: true}
    }
  );
});

app.factory('File', function($resource, ENV) {
  return $resource(
    ENV.apiEndpoint + '/api/files/:f/:id',
    {f: '@f', id: '@id'},
    {
      'get':   {method: 'GET'},
      'post':  {method: 'POST'},
      'query': {method: 'GET', isArray: true}
    }
  );
});


app.factory('Folder', function($resource, ENV) {
  return $resource(
    ENV.apiEndpoint + '/api/folders/:f/:id',
    {f: '@f', id: '@id'},
    {
      'update':{method: 'PUT'},
      'get':   {method: 'GET', isArray: false},
      'post':  {method: 'POST'},
      'query': {method: 'GET', isArray: true}
    }
  );
});

app.factory('authInterceptor', function($rootScope, $q, $window) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
      }
      return config;
    },
    responseError: function(response) {
      console.log(response);
      if(response.status === 401) {
        $window.location.href = '/';
      }
      return response || $q.when(response);
    }
  };
});

app.factory('Plan', function($resource, ENV){
  return $resource(
    ENV.apiEndpoint + '/api/plans/:id',
    {id: '@id'},
    {
      'get':    {method:'GET'},
      'save':   {method:'POST'},
      'update': {method: 'PUT'},
      'query':  {method:'GET', isArray:true},
      'remove': {method:'DELETE'},
      'custom': {method:'GET'}
    });
});

app.factory('Planp', function($resource, ENV){
  return $resource(
    ENV.apiEndpoint + '/plans',
    {},
    {
      'query':  {method:'GET', isArray:true}
    });
});

app.filter('size', function() {
  return function(input, scale) {
    var out = "";
    var size = parseInt(input);
    if (isNaN(size)) return "0";
    var unit = ["o","Ko","Mo","Go","To"];
    var i = 0;
    while (size >= 1024) {
      i++;
      size = size/1024;
    }
    out = size.toFixed(scale) + ' ' + unit[i];
    return out;
  }
});