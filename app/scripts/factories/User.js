'use strict';

var service = angular.module('cubbyApp');

service.factory('User', function($resource){
  return $resource(
    'http://api.cubby.schrepel.me/users/:id',
    {},
    {
      'get':    {method:'GET'},
      'save':   {method:'POST'},
      'query':  {method:'GET', isArray:true},
      'remove': {method:'DELETE'},
      'delete': {method:'DELETE'}
    });
});

service.factory('UserLogin', function($resource){
  return $resource(
    'http://api.cubby.schrepel.me/users/login',
    {},
    {
      'post':   {method:'POST'}
    });
});