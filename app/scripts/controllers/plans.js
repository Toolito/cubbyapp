'use strict';

app.controller('PlansCtrl', function ($rootScope, fileUpload, Api, File, $scope, $compile, $location, User, Plan, $route) {
  $rootScope.app = true;
  $scope.$watch(function() { return $route.current.css; }, function(value) {
    $scope.css = value;
  });
  $scope.plans = Plan.query();

  $scope.test = 'azerty';

  $scope.auth = Api.get({f: 'profile'});

  $scope.upload = function(path) {
    var fileList = $scope.myFiles;
    if (fileList.length == 0) {
      console.log("Please browse for one or more files.");
    } else {
      for (var i = 0; i < fileList.length; i++) {
        var file = fileList[i];
        fileUpload.uploadFileToUrl(file, path);
      }
    }
    //File.post({f: 'upload'});
  };

  $scope.template = "/views/partials/plans/buttonDefault.html";

  $scope.subscribe = function(plan){

  };

  $scope.back = function(){
    $scope.template = "views/partials/plans/buttonDefault.html"
  };


});


