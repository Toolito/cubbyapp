'use strict';

app.controller('SpaceCtrl', function (ENV, $route, $rootScope, $scope, Folder) {
  $rootScope.app = true;
  $rootScope.type = 'space';

  $scope.$watch(function() { return $route.current.css; }, function(value) {
    $scope.css = value;
  });

});