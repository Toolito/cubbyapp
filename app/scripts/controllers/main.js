'use strict';

app.controller('MainCtrl', function ($route, $scope, $http, User, Api, Plan, $window, $rootScope) {
  $rootScope.app = true;
  $scope.$watch(function() { return $route.current.css; }, function(value) {
    $scope.css = value;
  });
  $scope.auth = Api.get({f: 'profile'});
  console.log($scope.auth);

  $scope.updateUser = function(attr, data){
    console.log(attr + ' ' + data);
    if(attr == 'name')
    {
      $scope.auth.name = data;
    }
    else if(attr == 'email')
    {
      $scope.auth.email = data;
    }
    $scope.auth.$update({f: 'profile'});
  }

  $scope.plans = Plan.query();

});
