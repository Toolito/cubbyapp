'use strict';

app.controller('AdminCtrl', function ($route, $scope, $http, User, Api, Plan, $window, $rootScope) {
  $rootScope.app = true;
  $scope.$watch(function() { return $route.current.css; }, function(value) {
    $scope.css = value;
  });
  $scope.plans = Plan.query();

  $scope.updatePlan = function(id, attr, data){
    console.log(attr + ' '+data);
    var plan = Plan.get({},{'id': id});

    plan[attr] = data;
    plan.$update();
  };

  $scope.removePlan = function(idx){
    var plan = $scope.plans[idx];
    Plan.delete({}, {'id': plan._id});
    $scope.plans.splice(idx, 1);
  };

});