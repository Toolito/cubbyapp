'use strict';

app.controller('VitrineCtrl', function (ENV, $route, Planp, $scope, $compile, $location, User, $http, $window, $rootScope) {
  $rootScope.app = false;
  $scope.$watch(function() { return $route.current.css; }, function(value) {
    $scope.css = value;
  });
  $scope.isAuthenticated = false;
  $scope.message = '';
  $scope.plans = Planp.query();

  $scope.login = function(user) {
    $http
      .post(ENV.apiEndpoint + '/authenticate', user)
      .success(function (data, status, headers, config) {
        console.log(data);
        delete $window.sessionStorage.token;
        $window.sessionStorage.token = data.token;
        $window.sessionStorage.uid = data.uid;
        $location.path('/space');
      })
      .error(function (data, status, headers, config) {
        delete $window.sessionStorage.token;

        $scope.message = 'Error: invalid username or password';
      })
  };

  $scope.showRegister = function(){
    $('#lrForm').hide();
    $('#rForm').show();
    return false;
  };

  $('.login, .mask, .pack-order').click(function(){
    if ($('.login_form').is(':visible'))
    {
      $('.mask').fadeOut();
      $('.login_form').slideUp();
    }
    else
    {
      $('.mask').fadeIn();
      $('.login_form').slideDown();
    }
  });

  $scope.register = function(user){
    $http.post(ENV.apiEndpoint + '/register', user)
      .success(function (data, status, headers, config){
        var me = {email: user.email, password: user.password};
        $scope.login(me);
      })
      .error(function (data, status, headers, config){
        $scope.message = 'Error: cannot register';
      });
  };

  $scope.getUsers = function(){
    User.remove({"id": "52c079e7b13d3f9d03e14a66"}, function(data) {
      console.log(data);
    });
  };
});
