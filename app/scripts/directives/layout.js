'use strict';

app.directive("drag", ["$rootScope", function($rootScope) {

  function dragStart(evt, element, dragStyle) {
    element.addClass("success");
    //evt.dataTransfer.setData("id", evt.target.id);
    //evt.dataTransfer.effectAllowed = 'move';
  };
  function dragEnd(evt, element, dragStyle) {
    element.removeClass("success");
  };

  return {
    restrict: 'A',
    link: function(scope, element, attrs)  {
      attrs.$set('draggable', 'true');
      scope.element = scope[attrs['drag']];
      scope.draggedIndex = scope[attrs["index"]];
      //scope.dragStyle = attrs["dragstyle"];
      element.bind('dragstart', function(evt) {
        $rootScope.dragged = {
          index: scope.draggedIndex,
          element: scope.element
        }
        dragStart(evt, element, scope.dragStyle);
      });
      element.bind('dragend', function(evt) {
        dragEnd(evt, element, scope.dragStyle);
      });
    }
  }
}]);

app.directive("drop", ['$rootScope', function($rootScope, Folder) {

  function dragEnter(evt, element, dropStyle) {
    evt.preventDefault();
    element.addClass("success");
  };
  function dragLeave(evt, element, dropStyle) {
    element.removeClass("success");
  };
  function dragOver(evt) {
    evt.preventDefault();
  };
  function drop(evt, element, dropStyle) {
    evt.preventDefault();
    element.removeClass("success");
  };

  return {
    restrict: 'A',
    link: function(scope, element, attrs)  {
      scope.elementDrop = scope[attrs['drop']];
      console.log(scope.elementDrop);

      element.bind('dragenter', function(evt) {
        dragEnter(evt, element, scope.dropStyle);
      });
      element.bind('dragleave', function(evt) {
        dragLeave(evt, element, scope.dropStyle);
      });
      element.bind('dragover', dragOver);
      element.bind('drop', function(evt) {
        drop(evt, element, scope.dropStyle);
        $rootScope.$broadcast('dropEvent', $rootScope.dragged, scope.elementDrop);
      });
    }
  }
}]);

app.directive('toggle', function(){
  return {
    restruct: 'A',
    link: function(scope, elem, attrs) {
      var el = angular.element(elem);
      elem.on('click', function(e){
        el.toggleClass('active');

      })
    }
  }
})

app.directive('a', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs) {
      if(attrs.href === '' || attrs.href === '#' || attrs.class == 'dropdown-toggle'){
        elem.on('click', function(e){
          e.preventDefault();
        });
      }
    }
  };
});

app.directive('li', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs){
      if(attrs.class == 'list-grou-item'){
        elem.on('click', function(e){
          e.preventDefault();
        })
      }
    }
  };
});

app.directive('myAccount', function(){
  return {
    restrict: 'A',
    link: function(scope, elem, attrs){
      elem.on('click', function(e){
        var el = elem.next();
        el.toggleClass('hide');
        console.log(el);
      });
    },
    templateUrl: 'views/layout/myAccount.html',
    controller: function($scope){
      $scope.items = [
        'The first choice!',
        'And another choice for you.',
        'but wait! A third!'
      ];

      $scope.status = {
        isopen: false
      };

      $scope.toggled = function(open) {
        console.log('Dropdown is now: ', open);
      };

      $scope.toggleDropdown = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
      };
    }
  };
});

app.directive('appHeader', function(){
  return {
    restrict: 'E',
    templateUrl: 'views/layout/header.html',
    controller: function($location, Api, $scope, $rootScope, $window) {
      $scope.init = function(){

        this.nav = '/space';
      };

      $scope.disconnect = function(){
        delete $window.sessionStorage.token;
        $location.path("/");
      };

      $rootScope.auth = Api.get({f: 'profile'}, function(){
        $scope.links = [
          {
            id: 1,
            name: 'Espace',
            url: '/#/space',
            path: '/space'
          },
          {
            id: 2,
            name: 'Plans',
            url: '/#/plans',
            path: '/plans'
          },
          {
            id: 3,
            name: 'Partage',
            url: '/#/partage',
            path: '/partage'
          }
        ];
        if($rootScope.auth.admin){
          var adminLink = {
            id: 4,
            name: 'Admin',
            url: '/#/admin',
            path: '/admin'
          };
          $scope.links.push(adminLink);
        };
      });
      $scope.updateUser = function(attr, data){
        console.log(attr + ' ' + data);
        if(attr == 'name')
        {
          $scope.auth.name = data;
        }
        else if(attr == 'email')
        {
          $scope.auth.email = data;
        }
        $rootScope.auth.$update({f: 'profile'});
      };



      this.nav = $location.path();

      this.isSet = function(checkNav) {
        return this.nav === checkNav;
      };

      this.setNav = function(setNav) {
        this.nav = setNav;
      };

    },
    controllerAs: 'nav'
  };
});
