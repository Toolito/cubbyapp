app.directive('fileSystem', function(){
  return {
    restrict: 'E',
    templateUrl: 'views/layout/filesystem.html',
    scope: {
      type: '@type'
    },
    controller: function(ENV, $scope, File, Folder, User, $rootScope, Link, $http, $modal, $log){
      console.log($scope.type);
      if($scope.type != 'space'){
        $rootScope.currentName = '#';
        $scope.rootPath = ':';
        $rootScope.currentTab = [];
        //$scope.prevFolder;
        //$scope.currentFolder;
        $scope.folders = Folder.query({},{'f': 'share', 'id': $scope.type});
      }else {
        $rootScope.currentName = '#';
        $scope.rootPath = ':';
        $rootScope.currentTab = [];
        $scope.prevFolder;
        $scope.currentFolder = Folder.get({}, {'f': 'path', 'id': $scope.rootPath}, function(){
          $scope.currentFolderId = $scope.currentFolder._id;
        });
        $scope.folders = Folder.query({},{'f': 'path', 'id': $rootScope.currentName});
      }
      $scope.refresh = function(){
        console.log($scope.type)
        var path = $rootScope.currentName.slice(0);
        path = path.replace(/\//g,'.');
        $scope.folders = Folder.query({},{'f': 'path', 'id': path});
        $scope.currentFolder = Folder.get({}, {'f': $scope.currentFolderId});
      };

      $scope.changeFolder = function(folder){
        if($rootScope.currentName == '#'){
          $rootScope.currentName = folder.name;
        } else {
          $rootScope.currentName = $rootScope.currentName + '/' + folder.name;
        }
        $rootScope.currentTab.push(folder.name);
        var prevId = $scope.currentFolderId;
        $scope.prevFolder = Folder.get({}, {'f': prevId});
        $scope.currentFolderId = folder._id;
        $scope.refresh();
      };

      $scope.previousFolder = function(prevFolder){
        if($rootScope.currentTab.length == 1){
          $rootScope.currentTab = [];
          $rootScope.currentName = "#";
        } else {
          $rootScope.currentTab.splice(1);
          $rootScope.currentName = $scope.currentTab.join('/');
        }
        $scope.currentFolderId = prevFolder._id;

        $scope.refresh();
      };

      $scope.addFolder = function(name){
        console.log(name);
        var path = $rootScope.currentName;
        var folder = new Folder();
        folder.path = path;
        folder.name = name;
        folder.$save();
        $scope.folders.push(folder);
        $scope.folderName = "";
      };

      $scope.updateFolder = function(id, attr, data){
        console.log(attr + ' ' + data);
        if(attr == 'name')
        {
          var folder = Folder.get({},{'f': id});
          folder.name = data;
          if(typeof folder.path == 'undefined')
            folder.path = '#';
          folder.$update();
        }
      };

      $scope.removeFolder = function(idx){
        var folder = $scope.folders[idx];
        Folder.delete({}, {'f': folder._id});
        $scope.folders.splice(idx, 1);
      };

      $scope.shareFile = function(file, event){
        var object = {
          id : file._id
        };
        $http
          .post(ENV.apiEndpoint + '/api/links', object)
          .success(function (data, status, headers, config) {
            console.log(data);
            var link = ENV.apiEndpoint + '/links/' + data.secret;
            $scope.shareLink = link;
          })
          .error(function (data, status, headers, config) {
          })
      };



      $scope.downloadFile = function(file){

        $http.get(ENV.apiEndpoint+'/api/files/download/'+file._id,
          {responseType: "arraybuffer"}
        ).success(function(data, status, headers) {

            var octetStreamMime = "application/octet-stream";
            headers = headers();
            var filename = file.name;
            var contentType = file.type;
            if(navigator.msSaveBlob)
            {
              var blob = new Blob([data], { type: contentType });
              navigator.msSaveBlob(blob, filename);
              console.log("SaveBlob Success");
            }
            else
            {
              var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
              if(urlCreator)
              {
                var link = document.createElement("a");
                if("download" in link)
                {
                  var blob = new Blob([data], { type: contentType });
                  var url = urlCreator.createObjectURL(blob);
                  link.setAttribute("href", url);
                  link.setAttribute("download", filename);
                  var event = document.createEvent('MouseEvents');
                  event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                  link.dispatchEvent(event);

                  console.log("Download link Success");
                } else {
                  var blob = new Blob([data], { type: octetStreamMime });
                  var url = urlCreator.createObjectURL(blob);
                  window.location = url;

                  console.log("window.location Success");
                }
              } else {
                console.log("Not supported");
              }
            }
          })
          .error(function(data, status) {
            $scope.info = "Request failed with status: " + status;
          });
      };


      $scope.removeFile = function(idx, id){
        File.delete({},{'f': id});
        $scope.currentFolder.files.splice(idx, 1);
      };

      $rootScope.$on('dropEvent', function(evt, dragged, dropped) {
        var newPath = '';
        var newPathTab = $rootScope.currentTab.slice(0);

        console.log('dragged ' +dragged.element.type)
        if(dragged.element.type && dropped){ // fichier
          dropped.files.push(dragged.element);
          console.log('newpath ' + dropped.path);

          Folder.update({'f': dropped._id}, dropped);

          $scope.currentFolder.files.splice(dragged.index, 1);
          Folder.update({'f': $scope.currentFolder._id}, $scope.currentFolder);
        }else if (dragged.element.type && typeof dropped == 'undefined'){
          $scope.prevFolder.files.push(dragged.element);
          Folder.update({'f': $scope.prevFolder._id}, $scope.prevFolder);

          $scope.currentFolder.files.splice(dragged.index, 1);
          Folder.update({'f': $scope.currentFolder._id}, $scope.currentFolder);
        }else {
          if(dropped){ //c'est un dossier
            newPathTab.push(dropped.name);
            newPath = newPathTab.join('/');
          }else { //previous
            if(newPathTab.length == 1){
              newPath = '#';
            }
            else{
              newPathTab.splice(1);
              newPath = newPathTab.join('/');
            }
          }
          var folder = Folder.get({}, {'f': 'move', 'id': dragged.element._id});
          folder.path = newPath;
          folder.$update();
          $scope.folders.splice(dragged.index, 1);
        }
      });

      //REGLAGES DOSSIERS PERMISSIONS
      $scope.openSettings = function (folder) {

        var users = User.query({});
        var modalInstance = $modal.open({
          templateUrl: 'views/partials/modalSettingsFolder.html',
          controller: 'ModalInstanceCtrl',
          size: 'lg',
          resolve: {
            folder: function(){
              return folder;
            },
            users: function () {
              return users;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

    }
  }
})
  .controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'folder', 'users', 'Folder', function ($scope, $modalInstance, folder, users, Folder) {

  $scope.users = users;
  $scope.folder = folder;
  $scope.selectedUsers = [];


  $scope.itemClass = function(user, permission) {
    console.log($scope.folder);
    if(permission == 'rw'){
      return _.contains($scope.folder.guestsWrite, user._id) ? 'active' : undefined;
    } else if (permission == 'ro'){
      return _.contains($scope.folder.guestsRead, user._id) ? 'active' : undefined;
    } else {
      return undefined;
    }
  };

  $scope.togglePermission = function(user, permission){
    var object = {
      id: user._id,
      permission: permission
    };
    if(_.contains($scope.folder.guestsWrite, user._id) && permission == 'rw'){
      object.permission = 'rew';
    } else if(_.contains($scope.folder.guestsRead, user._id) && permission == 'ro'){
      object.permission = 're';
    }
    $scope.folder = Folder.update({'f': 'share', 'id': $scope.folder._id}, object);
  };

  $scope.ok = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}]);