'use strict';

app.directive('upload', function(){
  return {
    restrict: 'E',
    templateUrl: 'views/layout/upload.html',
    controller: function($scope, $rootScope, fileUpload){
      $scope.upload = function(path){
        var fileList = $scope.myFiles;
        if (fileList.length == 0) {
          console.log("Please browse for one or more files.");
        } else {
          for (var i = 0; i < fileList.length; i++) {
            var file = fileList[i];
            fileUpload.uploadFileToUrl(file, path);
          }
        }
      }

      var dropbox = document.getElementById("dropzone")

      // init event handlers
      function dragEnterLeave(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
          $scope.dropText = 'Drop files here...'
          $scope.dropClass = ''
        })
      }
      dropbox.addEventListener("dragenter", dragEnterLeave, false)
      dropbox.addEventListener("dragleave", dragEnterLeave, false)
      dropbox.addEventListener("dragover", function(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        var clazz = 'not-available'
        var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
        $scope.$apply(function(){
          $scope.dropText = ok ? 'Drop files here...' : 'Only files are allowed!'
          $scope.dropClass = ok ? 'over' : 'not-available'
        })
      }, false)
      dropbox.addEventListener("drop", function(evt) {
        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
          $scope.dropText = 'Glissez vos fichiers ici...'
          $scope.dropClass = ''
        })
        var files = evt.dataTransfer.files
        if (files.length > 0) {
          $scope.$apply(function(){
            $scope.files = [];
            var path = $rootScope.currentName;
            if($rootScope.currentName == '#' || typeof $rootScope.currentName == 'undefined'){
              path = '';
            }
            console.log('pathhhh '+path);
            for (var i = 0; i < files.length; i++) {
              console.log('PATH: ' +path)
              $scope.files.push(files[i]);
              fileUpload.uploadFileToUrl(files[i], path);
            }
          })
        }
      }, false)
    }
  }
})

app.service('fileUpload', ['$http','ENV', '$q', '$rootScope', '$upload', function ($http, ENV, $q, $rootScope, $upload) {
  this.uploadFileToUrl = function(file, path){
    var fd = new FormData();
    fd.append('file', file);
    fd.append('path', path);
    $rootScope.currentFile = file;
    $rootScope.currentFile.progress = true;
    var uploadUrl = ENV.apiEndpoint + '/api/files/upload';
/*    $upload.upload({
      url: uploadUrl, //upload.php script, node.js route, or servlet url
      method: 'POST',
      headers: {'Content-Type': undefined},
      // withCredentials: true,
      file: file,
      data: path
    }).progress(function(evt) {
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      }).success(function(data, status, headers, config) {
        // file is uploaded successfully
        console.log('DATA ' +data);
      });*/
    $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    })
    .success(function(){
        $rootScope.currentFile.progress = false;
    })
    .error(function(){
    });
  };


}]);